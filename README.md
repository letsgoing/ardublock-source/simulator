# Simulator für letsgoING als Einbindung in Ardublock
## Ziel
Die Anwendung Ardublock, welche bei letsgoING eingesetzt wird, soll um einen Simulator erweitert werden.
Dieser soll es Schülern ermöglichen auch ohne Zugriff auf Hardware die Programme aus Ardublock zu simulieren. 
Neben dem Arduino ermöglicht er auch die Simulation kleiner Module, wie sie im letsgoING-Projekt eingesetzt werden.
## Programm
Der Simulator wird direkt in die Oberfläche von Ardublock integriert und aus dieser gestartet. Er verfügt über ein eigenes Fenster, in welchem die Ergebnisse der Simulation dargestellt werden.
Für eine direkte Integration wird die Anwendung in Java geschrieben.

Zu den geplanten Features gehören unter anderem:
-Ausgabe des SerialLogs
-Simulation der Basis-Blöcke
-Simulation der letsgoING-Module


##Verwendung
Dieses Java Projekt ist theoretisch alleine lauffähig, dies ist aber nicht sinnvoll, da so das Programm manuell in der main() programmiert werden muss.
Für die korrekte Verwendung muss sich im Eclipse Workspace auch das Ardublock, sowiew das Openblocks-Projekt befinden.
(möglicherweise werden auch Maven-Bibilotheken benötigt)
Der Simulator wird dann aus Ardublock gestartet und die main() dieses Projekts wird nicht benötigt.
Zur Einrichtung der anderen Projekte sollte die Anleitung in der Dokumentation verwendet werden.

## Dokumentation
Die Dokumentation liegt als PDF bei Anian Bühler oder Stefan Mack vor.
In diesem Ordner befindet sich noch die UML des Projektes. Zum öffnen wird das Programm DIA benötigt.

##Beteiligte:
Lucas Stratmann lucas.stratmann@student.reutlingen-university.de


