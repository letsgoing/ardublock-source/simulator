/**
 * 
 */
package tec.letsgoing.ardublock.simulator.view.modules;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import tec.letsgoing.ardublock.simulator.Simulator;
import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.arduino.Pin;

/**
 * Modul für einen Arduino Uno
 * 
 * @author Lucas
 *
 */
public class ArduinoUno extends Modul {
	Arduino arduino;
	private boolean ledOn;
	private int led13                   = 0;
	private JLabel labelPower;
	private JLabel chiplabel;
	private JLabel label13;
	private JLabel labelButton;
	private ImageIcon chipIcon;
	private ImageIcon chipIcon_temp;
	private JButton button;

	
	public ArduinoUno(ImageIcon _icon, Simulator simu, int _xscale, int _yscale) {
		//Die �bergebenen Skalierungswerte werden in das Objekt geschrieben
		this.xscale = _xscale; 
		this.yscale = _yscale;
		//Das Label f�r den Arduino wird erzeugt
		chiplabel = new JLabel();
		//Das Bild des Arduinos wird in chipIcon geladen
		chipIcon = _icon;
		//Die tempor�re Variable chipIcon_temp speichert das den Seitenverh�ltnissen entsprechend neu skalierte Bild
		chipIcon_temp = new ImageIcon(chipIcon.getImage().getScaledInstance(((int)(0.606*xscale)), ((int)(0.432*yscale)), Image.SCALE_SMOOTH));
		//das neue skalierte Bild wird in das chipLabel geschrieben
		chiplabel.setIcon(chipIcon_temp);
		
		//Der Reset-Button wird erzeugt und initialisiert
		button = new JButton();
		button.setActionCommand("reset");
		button.addActionListener(simu);
		button.setOpaque(false);
		button.setContentAreaFilled(false);
		button.setBorderPainted(false);
		
		//Ein Label f�r den Reset-Button wird erzeugt
		labelButton = new JLabel();
		labelButton.add(button);
		layerpane.add(labelButton, 0);
		
		// Label f�r die PowerLED
		labelPower = new JLabel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				
				Graphics2D ga = (Graphics2D) g;
				int transparancy = 0;
				if (ledOn)
					transparancy = 220;
				ga.setPaint(new Color(255, 255, 0, transparancy));
				ga.fillRect(0, 0,((int)(0.02066*xscale)), ((int)(0.01034*yscale)));
			}

		};
		
		// Label für die Pin 13 LED
		label13 = new JLabel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				Graphics2D ga = (Graphics2D) g;
				ga.setPaint(new Color(255, 255, 0, led13 / 4));
				ga.fillRect(0, 0, ((int)(0.02066*xscale)), ((int)(0.01034*yscale)));
			}

		};
		
		//Die update-Funktion wird mit den �bergebenene Werten der Seitenverh�ltnisse aufgerufen
		updateGUI(xscale, yscale);
		
		

	}
	
	@Override
	public void updateGUI(int _xscale, int _yscale) {
		
		
		
		this.xscale = _xscale; 
		this.yscale = _yscale;
		
		
		// Offset für den Arduino um diesen mehr mittig zu platzieren.
		int locx = ((int)(0.0516*xscale));
		int locy = ((int)(0.0517*yscale));
				
		layerpane.setPreferredSize(new Dimension(((int)(0.606*xscale)) + locx, ((int)(0.432*yscale)) + locy));
				
		chipIcon_temp.setImage(chipIcon.getImage().getScaledInstance(((int)(0.606*xscale)), ((int)(0.432*yscale)), Image.SCALE_SMOOTH));
				
		chiplabel.setSize(((int)(0.606*xscale)), ((int)(0.432*yscale)));				
		chiplabel.setLocation(locx, locy);
				
		layerpane.add(chiplabel, 0);

		//Position und Gr��e der PowerLED werden aktualisiert				
		labelPower.setLocation(((int)(0.5207*xscale))+locx, ((int)(0.13*yscale))+locy);
		labelPower.setSize(((int)(0.02066*xscale)), ((int)(0.01034*yscale)));
		layerpane.add(labelPower, 0);
				
		
		//Position und Gr��e der LED13 werden aktualisiert
		label13.setLocation(((int)(0.2655*xscale))+locx, ((int)(0.092*yscale))+locy);
		label13.setSize(((int)(0.02066*xscale)), ((int)(0.01034*yscale)));
		layerpane.add(label13, 0);

		//Gr��e und Position des Buttons werden aktualisiert
		button.setSize(((int)(0.0723*xscale)), ((int)(0.0724*yscale)));
								
		labelButton.setSize(((int)(0.062*xscale)), ((int)(0.0527*yscale)));
		labelButton.setLocation(((int)(0.06612*xscale))+locx, ((int)(0.00103*yscale))+locy);
		//Die Pin Positionen werden neu berechnet
		calculatePinPos(locx, locy);
	}
	

	public void updateModul(Pin pin) {
		if (pin == pins.get(0))
			ledOn = (pin.getValue() == 1023);
		if (pin == pins.get(1)) {
			led13 = pin.getValue();
		}
		
	}

	public boolean connect(Arduino _arduino) {
		Pin tmpPin = _arduino.getPin(20);
		this.addPin(tmpPin);
		tmpPin.setObserver(this);

		tmpPin = _arduino.getPin(13);
		this.addPin(tmpPin);
		tmpPin.setObserver(this);
		return true;
	}

	private void calculatePinPos(int _locx, int _locy) {
		
		Vector<Point> pins = new Vector<Point>();
		int offset = 0;
		for (int i = 15; i > 1; i--) {
			if (i < 8) {
				offset = -((int)(0.012*xscale));
			}
			pins.add(new Point(((int)(i * (0.02066*xscale))) + _locx + ((int)(0.2545*xscale)) + offset, _locy + ((int)(0.01965*yscale))));	
			
		}
		for (int i = 0; i < 6; i++) {
			pins.add(new Point(((int)(i * (0.02066*xscale))) + _locx + ((int)(0.46178*xscale)), _locy + ((int)(0.41054*yscale))));
//			pins.add(new Point(i * ((int)(0.02066*xscale)) + _locx + ((int)(0.46178*xscale)), _locy + ((int)(0.41054*yscale))));
		}
		// Dieser Code malt kleine Rechtecke an die PinPositionen um sie besser zu
		// überprüfen.
		/*
		 * JLabel label; for (Point pin:pins) { label= new JLabel() {
		 * 
		 * @Override public void paintComponent(Graphics g) { g.setColor(Color.RED);
		 * g.fillRect(0, 0, 10, 10); }
		 * 
		 * };
		 * 
		 * label.setLocation(pin.x,pin.y); label.setSize(10, 10); layerpane.add(label,
		 * 0); }
		 */
		
		this.setPinPos(pins);
	}

	

	
	

}
