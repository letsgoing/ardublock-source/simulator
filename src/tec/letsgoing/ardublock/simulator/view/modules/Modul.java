/**
 * 
 */
package tec.letsgoing.ardublock.simulator.view.modules;

import java.awt.Point;
import java.util.Observable;
import java.util.Observer;
import java.util.Vector;

import javax.swing.JLayeredPane;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.arduino.Pin;

/**
 * Oberklasse für alle Module
 * 
 * @author Lucas
 *
 */
public abstract class Modul implements Observer {
	private boolean active           = true;
	protected Vector<Pin> pins       = new Vector<Pin>();
	protected JLayeredPane layerpane = new JLayeredPane();
	protected Vector<Point> pinPos   = new Vector<Point>();
	protected int xscale; 
	protected int yscale;

	public void setState(boolean State) {
		active = State;
	}

	public boolean getState() {
		return active;
	}

	public void setPosition(Point Position) {
	}

	public Point getPosition() {
		return layerpane.getLocation();
	}

	/**
	 * Funktion welche aufgerufen wird, sobald sich ein Pin ändert.
	 */
	public void update(Observable Observable, Object arg1) {
		if (Observable instanceof Pin) {
			for (Pin p : pins) {
				if (p == (Pin) Observable)
					updateModul((Pin) Observable);
			}
		}

	}

	public abstract void updateModul(Pin pin);
	
	
	/**
	 * Funktion wird aufgerufen wenn sich die Gr��e des aktuellen Fensters �ndert und die Module werden entsprechend der neuen Fenstergr��e
	 * neu skaliert und angepasst.
	 * @param xscale
	 * Fenstergr��e in x
	 * @param yscale
	 * Fenstergr��e in y
	 */
	public abstract void updateGUI(int xscale, int yscale);

	public void addPin(Pin _pin) {
		pins.add(_pin);
	}

	public Vector<Point> getPinPos() {
		return pinPos;
	}

	public void setPinPos(Vector<Point> _pins) {
		pinPos = _pins;
	}

	public JLayeredPane getPane() {
		return layerpane;
	}

	public abstract boolean connect(Arduino arduino);

}
