/**
 * 
 */
package tec.letsgoing.ardublock.simulator.view.modules;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JToggleButton;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.arduino.Pin;

/**
 * Modul mit 3 Toogle Buttons
 * 
 * @author Lucas
 *
 */
public class Button extends Modul implements ActionListener, MouseListener {
	private int[] lastState            = { 0, 0, 0 };
	private ImageIcon iconOff;
	private ImageIcon iconOn;
	private ImageIcon chipIcon;
	private ImageIcon chipIcon_temp;
	private ImageIcon iconOff_temp;
	private ImageIcon iconOn_temp;
	//private JToggleButton but1, but2, but3;
	private JLabel chiplabel;
	private JLabel label1;
	private JLabel label2;
	private JLabel label3;
	private LockButton but1;
	private LockButton but2;
	private LockButton but3;
	

	
	
	
	private class LockButton extends JButton {
		private static final long serialVersionUID = 1L;
		private boolean clicked                    = false;
		private boolean locked                     = false;
		private int pin;
		private int value                          = 0;
		private static final String BTN_TOOLTIP    = "Linksklick -> Taster | Rechtsklick -> Schalter";

		LockButton(int _pin){//ImageIcon _iconOn, ImageIcon _iconOff){
			pin = _pin;
			this.setBorderPainted(false);
			this.setToolTipText(BTN_TOOLTIP);
		}
		
		public boolean isClicked() {
			return clicked;
		}

		public void setClicked(boolean clicked) {
			this.clicked = clicked;
		}

		public boolean isLocked() {
			return locked;
		}

		public void setLocked(boolean locked) {
			this.locked = locked;
		}
		
		@SuppressWarnings("unused")
		public int getValue() {
			return value;
		}

		public void setValue(int value) {
			this.value = value;
			lastState[pin] = value; //TODO: remove when updateModule is updated
			pins.get(pin).setValue(value);
			pins.get(pin).getValue();
		}
		
		@Override
		public void setSize(int width, int height) {
			super.setSize(width, height);
		}
		
		public void update(){
			if (isClicked() || isLocked()) {
				setIcon(iconOn_temp);
				setValue(1023);
			} else {
				setIcon(iconOff_temp);
				setValue(0);
			}
		}
	}
	
	public Button(ImageIcon _icon, ImageIcon _icon1, ImageIcon _icon2, int _xscale, int _yscale) {
		//Die �bergebenen Skalierungswerte werden in das Objekt geschrieben
		this.xscale = _xscale; 
		this.yscale = _yscale;
		chipIcon = _icon;
		iconOff = _icon1;
		iconOn = _icon2;
		iconOff_temp = iconOff;
		iconOn_temp = iconOn;
		chiplabel = new JLabel();
		but1 = new LockButton(0);//JToggleButton();
		//but1.setActionCommand("0");
		//but1.addActionListener(this);
		but1.addMouseListener(this);
					
		but2 = new LockButton(1);//JToggleButton();
		//but2.setActionCommand("1");
		//but2.addActionListener(this);
		but2.addMouseListener(this);
		
		but3 = new LockButton(2);//JToggleButton();
		//but3.setActionCommand("2");
		//but3.addActionListener(this);
		but3.addMouseListener(this);
		
		label1 = new JLabel();						
		label2 = new JLabel();				
		label3 = new JLabel();
		//Das neu skalierte Icon wird in die tempor�re Variable chipIcon_temp geladen
		chipIcon_temp = new ImageIcon(chipIcon.getImage().getScaledInstance(((int)(0.3037*xscale)), ((int)(0.304*yscale)), Image.SCALE_SMOOTH));
		//Das skalierte Icon wird als Icon des chipLabels gesetzt.
		chiplabel.setIcon(chipIcon_temp);
		iconOff_temp = new ImageIcon(iconOff.getImage().getScaledInstance(((int)(0.0723*xscale)), ((int)(0.0724*yscale)), Image.SCALE_SMOOTH));
		iconOn_temp = new ImageIcon(iconOn.getImage().getScaledInstance(((int)(0.0723*xscale)), ((int)(0.0724*yscale)), Image.SCALE_SMOOTH));
		
		but1.setIcon(iconOff_temp);
		but2.setIcon(iconOff_temp);
		but3.setIcon(iconOff_temp);
		//eine Aktualisierung des Moduls wird aufgerufen
		updateGUI(xscale, yscale);
		
	
	}
	
	public void updateGUI(int _xscale, int _yscale){
		
		this.xscale = _xscale; 
		this.yscale = _yscale;
		
		//Das Icon wird gem�� den neuen Seitenverh�ltnissen neu skaliert und geladen
		layerpane.setPreferredSize(new Dimension(((int)(0.3037*xscale)), ((int)(0.304*yscale))));		
		chipIcon_temp.setImage(chipIcon.getImage().getScaledInstance(((int)(0.3037*xscale)), ((int)(0.304*yscale)), Image.SCALE_SMOOTH));		
		chiplabel.setSize(((int)(0.3037*xscale)), ((int)(0.304*yscale)));
		
		//Die Gr��e und Position des Buttons 1 wird aktualisiert
		but1.setSize(((int)(0.0723*xscale)), ((int)(0.0724*yscale)));				
		label1.setSize(((int)(0.0723*xscale)), ((int)(0.0724*yscale)));
		label1.setLocation(((int)(0.02996*xscale)), ((int)(0.06618*yscale)));
		
		//Die Icons On und Off werden neu skaliert und in die tempor�ren Variablen geschrieben
		iconOff_temp.setImage(iconOff.getImage().getScaledInstance(((int)(0.0723*xscale)), ((int)(0.0724*yscale)), Image.SCALE_SMOOTH));
		iconOn_temp.setImage(iconOn.getImage().getScaledInstance(((int)(0.0723*xscale)), ((int)(0.0724*yscale)), Image.SCALE_SMOOTH));

	
		
		//Die Gr��e und Position des Buttons 2 wird aktualisiert
		but2.setSize(((int)(0.0723*xscale)), ((int)(0.0724*yscale)));			
		label2.setSize(((int)(0.0723*xscale)), ((int)(0.0724*yscale)));
		label2.setLocation(((int)(0.1147*xscale)), ((int)(0.03619*yscale)));
		
		//Die Gr��e und Position des Buttons 3 wird aktualisiert
		but3.setSize(((int)(0.0723*xscale)), ((int)(0.0724*yscale)));						
		label3.setSize(((int)(0.0723*xscale)), ((int)(0.0724*yscale)));
		label3.setLocation(((int)(0.200*xscale)), ((int)(0.06618*yscale)));

		label1.add(but1);
		label2.add(but2);
		label3.add(but3);
		layerpane.add(chiplabel, 0);
		layerpane.add(label1, 0);
		layerpane.add(label2, 0);
		layerpane.add(label3, 0);
		//Die Positionen der Pins werden neu berechnet
		calculatePinPos();
	}

	public void updateModul(Pin pin) {
		int pinNumber = 0;
		if (pin == pins.get(1))
			pinNumber = 1;
		else if (pin == pins.get(2))
			pinNumber = 2;
		if (pins.get(pinNumber).getValue() != lastState[pinNumber]) {
			pins.get(pinNumber).setValue(lastState[pinNumber]);
		}
	}

	public boolean connect(Arduino arduino) {
		Pin tmpPin = arduino.getPin(4);
		this.addPin(tmpPin);
		tmpPin.setObserver(this);

		tmpPin = arduino.getPin(3);
		this.addPin(tmpPin);
		tmpPin.setObserver(this);

		tmpPin = arduino.getPin(2);
		this.addPin(tmpPin);
		tmpPin.setObserver(this);

		return true;
	}

	/**
	 * Funktion reagiert auf die Mausklicks und ändert das Icon
	 */
	public void actionPerformed(ActionEvent arg0) {
		Integer pin = Integer.parseInt(arg0.getActionCommand());
		boolean bool = ((JToggleButton) arg0.getSource()).isSelected();
		if (bool) {
			((JToggleButton) arg0.getSource()).setIcon(iconOn_temp);
		} else {
			((JToggleButton) arg0.getSource()).setIcon(iconOff_temp);
		}

		int value = 0;
		if (bool)
			value = 1023;
		
		lastState[pin] = value;
		pins.get(pin).setValue(value);
	
		
	}

	private void calculatePinPos() {
		Vector<Point> pins = new Vector<Point>();
		for (int i = 0; i < 3; i++) {
			pins.add(new Point(((int)(i * (0.031*xscale))) + ((int)(0.1209*xscale)), ((int)(0.243*yscale))));
		}
		this.setPinPos(pins);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		
		if(!(arg0.getSource() instanceof LockButton)) {
			return;
		}
		
		LockButton button = (LockButton) arg0.getSource();
		
		if( arg0.getButton() == MouseEvent.BUTTON2 || arg0.getButton() == MouseEvent.BUTTON3 ) { //to activate double-Click add arg0.getClickCount()==2 ||
			button.setLocked(!button.isLocked());
			button.setClicked(false);
		}
		else {//to activate lock with double-Click add: if(arg0.getClickCount()==1) {
			if(!button.isLocked()) {
				button.setClicked(true);
			}
		}
		button.update();
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		
		if(!(arg0.getSource() instanceof LockButton)) {
			return;
		}
		LockButton button = (LockButton) arg0.getSource();
		button.setClicked(false);
		button.update();
	}
	
}
