/**
 * 
 */
package tec.letsgoing.ardublock.simulator.view.modules;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.arduino.Pin;

/**
 * Modul mit einer RGB LED
 * 
 * @author Lucas
 *
 */
public class RGB extends Modul {
	private int redValue              = 0;
	private int greenValue            = 0;
	private int blueValue             = 0;
	private int transparancy;
	private int tredValue;
	private int tgreenValue;
	private int tblueValue;
	private JLabel chiplabel;
	private JLabel ledlabel;
	private ImageIcon chipIcon;
	private ImageIcon chipIcon_temp;


	public RGB(ImageIcon _icon, int _xscale, int _yscale) {
		this.xscale = _xscale; 
		this.yscale = _yscale;
		
		chiplabel = new JLabel();
		chipIcon = _icon;
		
		ledlabel = new JLabel() {
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g) {
				Graphics2D ga = (Graphics2D) g;
				transparancy = (int) (Math.max(redValue, Math.max(greenValue, blueValue)) * 0.9);
				tredValue = 0;
				tgreenValue = 0;
				tblueValue = 0;
				if (transparancy != 0) {
					if (redValue > 0)
						tredValue = map(redValue);
					if (greenValue > 0)
						tgreenValue = map(greenValue);
					if (blueValue > 0)
						tblueValue = map(blueValue);
				}
				 //System.out.println(tredValue+" "+ tgreenValue+" "+ tblueValue+" "+
				 //transparancy);
				
				ga.setPaint(new Color(tredValue, tgreenValue, tblueValue, transparancy));
				ga.fillOval(0, 0, ((int)(0.07541*xscale)), ((int)(0.07549*yscale)));
				//System.out.println("blink");
			}

		};
		
		layerpane.add(chiplabel, 0);
		layerpane.add(ledlabel, 0);
		//das ImageIcon wird mit dem neu skalierten Bild geladen
		chipIcon_temp = new ImageIcon(chipIcon.getImage().getScaledInstance(((int)(0.3037*xscale)), ((int)(0.304*yscale)), Image.SCALE_SMOOTH));
		//Das Neue ImageIcon wird als Icon des chipLabels gesetzt
		chiplabel.setIcon(chipIcon_temp);
		//Eine Aktualisierung des Moduls wird aufgerufen
		updateGUI(xscale, yscale);
	
	}
	
	@Override
	public void updateGUI(int _xscale, int _yscale) {
						
		this.xscale = _xscale; 
		this.yscale = _yscale;
		
		// Erstellen der JLayerPane f�r das Modul
		layerpane.setPreferredSize(new Dimension(((int)(0.3037*xscale)), ((int)(0.304*yscale))));
		chipIcon_temp.setImage(chipIcon.getImage().getScaledInstance(((int)(0.3037*xscale)), ((int)(0.304*yscale)), Image.SCALE_SMOOTH));
		chiplabel.setSize(((int)(0.3037*xscale)), ((int)(0.304*yscale)));		
				
		// Setze Position des Labels
		ledlabel.setLocation(((int)(0.11*xscale)), ((int)(0.0383*yscale)));
		ledlabel.setSize(((int)(0.2066*xscale)), ((int)(0.2068*yscale)));
		
		//Die Pin-Positionen werden neu berechnet
		calculatePinPos();
	}

	/**
	 * Funktion, welche die Pin �nderungen entgegen nimmt und verarbeitet.
	 */
	public void updateModul(Pin pin) {
		if (pin == pins.get(0))
			redValue = pin.getValue() / 4;
		if (pin == pins.get(1))
			blueValue = pin.getValue() / 4;
		if (pin == pins.get(2))
			greenValue = pin.getValue() / 4;

	}

	/**
	 * Verbindet das RGB Modul mit seinen 3 Pins am Arduino
	 */
	public boolean connect(Arduino arduino) {
		// TODO Pins= R G B ? Aktuell RBG
		Pin tmpPin = arduino.getPin(11);
		this.addPin(tmpPin);
		tmpPin.setObserver(this);

		tmpPin = arduino.getPin(10);
		this.addPin(tmpPin);
		tmpPin.setObserver(this);

		tmpPin = arduino.getPin(9);
		this.addPin(tmpPin);
		tmpPin.setObserver(this);

		return true;
	}

	private int map(int v) {
		float vL = 0, vH = 255, tL = 200, tH = 255;
		return (int) (((v - vL) / (vH - vL)) * (tH - tL) + tL);
	}

	/**
	 * Funktion, welche die Positionen der Modulpins berechnet (in Pixeln für das
	 * Zeichnen der Verdrahtung)
	 */
	private void calculatePinPos() {
		Vector<Point> pins = new Vector<Point>();
		for (int i = 0; i < 3; i++) {
			pins.add(new Point(((int)(i * (0.03099*xscale))) + ((int)(0.12087*xscale)), ((int)(0.243*yscale))));
		}
		this.setPinPos(pins);
	}

	

	

}
