/**
 * 
 */
package tec.letsgoing.ardublock.simulator.view.modules;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.arduino.Pin;

/**
 * Modul mit einem Potentiometer
 * 
 * @author Lucas
 *
 */
public class Poti extends Modul implements ChangeListener, MouseWheelListener {
	int value                                 = 0;
	private JSlider slider;
	private JLabel chiplabel;
	private JLabel sliderlabel1;
	private JLabel sliderlabel2;
	private JLabel sliderlabel3;
	private ImageIcon chipIcon;
	private ImageIcon chipIcon_temp;
	private JPanel sliderPanel;
	private Hashtable<Integer, JLabel> labelTable;

	
	private static final String POTI_TOOLTIP = "Slider ziehen oder mit Mausrad verstellen";
	
	public Poti(ImageIcon _icon, int _xscale, int _yscale) {
		this.xscale = _xscale; 
		this.yscale = _yscale;
		chipIcon = _icon;
		chiplabel = new JLabel();
		labelTable = new Hashtable<Integer, JLabel>();
		slider = new JSlider(JSlider.HORIZONTAL, 0, 1023, 512);
		slider.setMajorTickSpacing(256);
		slider.setMinorTickSpacing(64);
		slider.setPaintTicks(true);		
		
		// Erzeuge eine Slider mit individueller Beschriftung
		sliderlabel1 = new JLabel("0%");
		sliderlabel2 = new JLabel("50%");
		sliderlabel3 = new JLabel("100%");
		labelTable.put(new Integer(10), sliderlabel1);
		labelTable.put(new Integer(512), sliderlabel2);
		labelTable.put(new Integer(1023), sliderlabel3);
		slider.setLabelTable(labelTable);
		slider.setPaintLabels(true);
		slider.setOpaque(false);
		slider.addChangeListener(this);
		slider.addMouseWheelListener(this);
		slider.setToolTipText(POTI_TOOLTIP);
		sliderPanel = new JPanel();
		sliderPanel.add(slider);
		sliderPanel.setOpaque(false);
		layerpane.add(chiplabel, 0);
		layerpane.add(sliderPanel, 0);
		//Das ImageIcon des Potis wird neu skaliert und tempor�r geladen
		chipIcon_temp = new ImageIcon(chipIcon.getImage().getScaledInstance(((int)(0.3037*xscale)), ((int)(0.304*yscale)), Image.SCALE_SMOOTH));
		chiplabel.setIcon(chipIcon_temp);
		//Eine Aktualisierung des Moduls wird aufgerufen
		updateGUI(xscale, yscale);
		

	}
	
	@Override
	public void updateGUI(int _xscale, int _yscale) {
		
		
		this.xscale = _xscale; 
		this.yscale = _yscale;
		
		layerpane.setPreferredSize(new Dimension(((int)(0.3037*xscale)), ((int)(0.304*yscale))));				
		chipIcon_temp.setImage(chipIcon.getImage().getScaledInstance(((int)(0.3037*xscale)), ((int)(0.304*yscale)), Image.SCALE_SMOOTH));
		chiplabel.setSize(((int)(0.3037*xscale)), ((int)(0.304*yscale)));
		
		//Die Schriftgr��e wird gem�� den Seitenverh�ltnissen neu berechnet
		sliderlabel1.setFont(new Font(sliderlabel1.getName(), Font.BOLD, ((int)(0.0115*xscale))));
		sliderlabel2.setFont(new Font(sliderlabel1.getName(), Font.BOLD, ((int)(0.0115*xscale))));
		sliderlabel3.setFont(new Font(sliderlabel1.getName(), Font.BOLD, ((int)(0.0115*xscale))));
		slider.setPreferredSize(new Dimension(((int)(0.186*xscale)), ((int)(0.05*yscale))));
		sliderPanel.setSize(((int)(0.2066*xscale)), ((int)(0.066*yscale)));
		sliderPanel.setLocation(((int)(0.04855*xscale)), ((int)(0.175*yscale)));
		//Die Pin-Positionen werden neu berechnet
		calculatePinPos();
	}

	public void updateModul(Pin arg0) {
		if (pins.get(0).getValue() != slider.getValue()) {
			pins.get(0).setValue(slider.getValue());
		}
	}

	public boolean connect(Arduino arduino) {
		this.addPin(arduino.getPin(0 + 14));
		arduino.getPin(0 + 14).setObserver(this);
		return true;
	}

	/**
	 * Funktion wird aufgerufen, wenn die Maus den Slider bewegt hat.
	 */
	@Override
	public void stateChanged(ChangeEvent arg0) {
		pins.get(0).setValue(slider.getValue());
	}

	/**
	 * Funktion wird aufgerufen, wenn das Mausrad bewegt wurde.
	 */
	@Override
	public void mouseWheelMoved(MouseWheelEvent arg0) {
		int notches = arg0.getWheelRotation();
		if (notches > 0) {
			slider.setValue(slider.getValue() + 10);
		} else {
			slider.setValue(slider.getValue() - 10);
		}
	}

	private void calculatePinPos() {
		Vector<Point> pins = new Vector<Point>();
		pins.add(new Point(((int)(0.1519*xscale)), ((int)(0.243*yscale))));
		this.setPinPos(pins);
	}

	

	

}
