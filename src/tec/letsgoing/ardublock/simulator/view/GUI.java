/**
 * 
 */
package tec.letsgoing.ardublock.simulator.view;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

//import edu.mit.blocks.workspace.Workspace;
import tec.letsgoing.ardublock.simulator.Simulator;
import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.view.modules.ArduinoUno;
import tec.letsgoing.ardublock.simulator.view.modules.Button;
import tec.letsgoing.ardublock.simulator.view.modules.Modul;
import tec.letsgoing.ardublock.simulator.view.modules.Poti;
import tec.letsgoing.ardublock.simulator.view.modules.RGB;

/**
 * Verwaltet die graphische Oberfläche.
 * 
 * @author Lucas
 *
 */
public class GUI extends JFrame implements Runnable, ActionListener {
	private static final int SCALING_OFFSET    = 100;  //wird f�r die Skalierung des Arduinos ben�tigt
	private static final int UPDATE_GAP        = 10;   //Legt fest wie gro� eine Fenstergr��en�nderung sein muss um eine Fensteraktualisierungen durchzuf�hren
	private static final long serialVersionUID = 1L;
	private static final int MAXIMUM_MESSAGES  = 100;  // Anzahl der Seriellen Nachrichten
	private static final int WINDOW_SIZE_MIN_X = 200;
	private static final int WINDOW_SIZE_MIN_Y = 200;
	
	private Vector<String> serialprint         = new Vector<String>();
	private Modul[] modules                    = new Modul[4];
	private volatile boolean stopFlag          = false;
	private JTextArea serialLog                = new JTextArea();
	private JPanel botControlPanel;
	private JPanel topControlPanel;
	private JPanel controlPanel;
	private JPanel panel;
	private JPanel topPanel;
	private JPanel modulPanel;
	private JButton goButton;
	private JButton stopButton;
	private JButton reloadButton;
	private JButton measButton;
	private JButton clearButton;
	private JCheckBox checkBox;
	private JScrollPane scrollPane;
	private Container mainPane;
	private int xscale;
	private int yscale;
	private int windowHeight;
	private int windowWidth;
	

	/**
	 * Konstruktor der Klasse GUI
	 * 
	 * @param simu Instanz des Simulators
	 */
	public GUI(Simulator simu, int _xscale, int _yscale, Point _windowLocation) {
		super("ArduBlock Simulator");
	
		
		//Das Fenster wird gem�� des kleineren Seitenverh�ltnisses erstellt
		if(_xscale <= _yscale - SCALING_OFFSET) {					
			xscale = _xscale;
			yscale = _xscale;					
		}								
		else {						
			xscale = _yscale - SCALING_OFFSET;
			yscale = _yscale - SCALING_OFFSET;						
		}	
		

		windowWidth = _xscale;
		windowHeight = _yscale;
		
		// Konstruktor der Module
				modules[0] = new RGB(new ImageIcon(getToolkit()
								.getImage(GUI.class.getResource("/tec/letsgoing/ardublock/simulator/img/PM31_RGB_LED.png"))), xscale, yscale);
				modules[1] = new Button(
						new ImageIcon(getToolkit()
								.getImage(GUI.class.getResource("/tec/letsgoing/ardublock/simulator/img/PM26_Taster.png"))),
						new ImageIcon(getToolkit()
								.getImage(GUI.class.getResource("/tec/letsgoing/ardublock/simulator/img/Taster_Off.png"))),
						new ImageIcon(getToolkit()
								.getImage(GUI.class.getResource("/tec/letsgoing/ardublock/simulator/img/Taster_On.png"))), xscale, yscale);
				modules[2] = new Poti(new ImageIcon(getToolkit()
						.getImage(GUI.class.getResource("/tec/letsgoing/ardublock/simulator/img/PM24_Potentiometer.png"))), xscale, yscale);
				modules[3] = new ArduinoUno(
						new ImageIcon(getToolkit()
								.getImage(GUI.class.getResource("/tec/letsgoing/ardublock/simulator/img/ArduinoUno.png"))),
						simu, xscale, yscale);

				//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //Vermutlich nicht
				// gew�nscht.
				this.setResizable(true); //TODO Muss in true ge�ndert werden wenn skalierbar
				
				createControlPanel(simu);
				mainPane = this.getContentPane();
				
				topPanel = new JPanel(new GridBagLayout());
				
				// Panel welches alle Module sowie die Verdrahtung enth�lt.
				modulPanel = new JPanel(new BorderLayout()) {
					private static final long serialVersionUID = 1L;

					@Override
					public void paint(Graphics g) {
						
						
							super.paint(g);
							drawConnections(g); // Zeichne Verdrahtung
						
					}
				
				};
				// Füge Module hinzu
				modulPanel.add(modules[0].getPane(), BorderLayout.WEST);
				modulPanel.add(modules[1].getPane(), BorderLayout.CENTER);
				modulPanel.add(modules[2].getPane(), BorderLayout.EAST);
				modulPanel.add(modules[3].getPane(), BorderLayout.PAGE_END);
				topPanel.add(modulPanel);
				mainPane.add(topPanel, BorderLayout.NORTH);
				//Panel der Buttons auf der rechten Seite wird ausgeblendet
				//mainPane.add(createControlPanel(simu), BorderLayout.EAST);
				mainPane.add(createSerialLog(), BorderLayout.PAGE_END);
				
				if(_windowLocation != null) {
					setLocation(_windowLocation);
				}
				//Die Fenstergr��e wird mit der dem Konstruktor �bergebenen Gr��e festgelegt				
				setSize(_xscale, _yscale);
				//Eine Minimum Fenstergr��e wird festgelegt
				setMinimumSize(new Dimension(WINDOW_SIZE_MIN_X, WINDOW_SIZE_MIN_Y));
				// this.setLocation(-1300, 0); //M�glichkeit die Renderingposition festzulegen
				setVisible(true);
				//Eine Aktualisierung der GUI gem�� den �bergebenen Seitenverh�ltnissen wird aufgerufen.
				updateGUI(xscale, yscale);
			
				
				//ComponentListener wird hinzugef�gt, der auf das Vergr��ern des Fenster reagiert und die GUI neu l�dt
		this.addComponentListener(new ComponentListener() {
			@Override
			public void componentResized(ComponentEvent e) {
					

					//Das Fenster soll erst dann upgedatet werden wenn eine Gr��en�nderung gr��er 5 stattgefunden hat
					//um zu verhindern dass die update Funktion durch ein zu h�ufiges Aufrufen das Programm aufh�ngt.
					if((Math.abs(windowHeight - getHeight()) > UPDATE_GAP) || (Math.abs(windowWidth - getWidth()) > UPDATE_GAP)) {
							
							//Die aktuelle Fenstergr��e wird abgespeichert
							windowHeight = getHeight();
							windowWidth = getWidth();

							
							//GUI wird nur gleichm��ig skaliert mit den Gr��en des kleineren Seitenverh�tlnisses
							if(windowWidth <= windowHeight - SCALING_OFFSET) {					
								xscale = windowWidth;
								yscale = windowWidth;					
							}								
							else {						
								xscale = windowHeight - SCALING_OFFSET;
								yscale = windowHeight - SCALING_OFFSET;						
							}		
							
								//GUI wird upgedatet
								updateGUI(xscale, yscale);
								
								
					}
					
				
			}

			@Override
			public void componentMoved(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void componentShown(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});		
	}
	

	/**
	 * Die Funktion wird aufgerufen wenn sich die Fenstergr��e ge�ndert hat um den Inhalt zu skalieren
	 * @param xscale
	 * Fenstergr��e in x
	 * @param yscale
	 * Fenstergr��e in y
	 */
	public void updateGUI(int _xscale, int _yscale) {
		
	
		//Jedes Modul wird mit den neuen Seitenverh�tlnissen aktualisiert
		for(int i = 0; i < 4; i++) {
			
			modules[i].updateGUI(_xscale, _yscale);
			
		}
		
	
		//Wird ben�tigt um kleine Anzeigefehler zu beseitigen
		super.setVisible(true);
		//Die H�he des SerialLog wird gem�� den Seitenverh�ltnissen angepasst
		updateSerialLogHeight(getHeight()-topPanel.getHeight());	
		
		
		
	}
	
	
	
	
	

	/**
	 * Erzeugt das Controlpanel mit den Steuerungsknöpfen
	 * 
	 * @param simu Instanz des Simulators
	 * @return JPanel mit allen Kn�pfen
	 */
	private JPanel createControlPanel(Simulator simu) {
		
		
		controlPanel = new JPanel(new BorderLayout());
		topControlPanel = new JPanel(new GridLayout(2,1));
		botControlPanel = new JPanel(new GridLayout(2,1));
		
	

		goButton = new JButton();
		stopButton = new JButton();
		reloadButton = new JButton();
		measButton = new JButton();
		
		goButton.setIcon(new ImageIcon(
				getToolkit().getImage(GUI.class.getResource("/tec/letsgoing/ardublock/simulator/img/Play.png"))));
		stopButton.setIcon(new ImageIcon(
				getToolkit().getImage(GUI.class.getResource("/tec/letsgoing/ardublock/simulator/img/Stop.png"))));
		reloadButton.setIcon(new ImageIcon(
				getToolkit().getImage(GUI.class.getResource("/tec/letsgoing/ardublock/simulator/img/Reload.png"))));
		measButton.setIcon(new ImageIcon(
				getToolkit().getImage(GUI.class.getResource("/tec/letsgoing/ardublock/simulator/img/Measure.png"))));
		
		goButton.addActionListener(simu);
		stopButton.addActionListener(simu);
		reloadButton.addActionListener(simu);
		measButton.addActionListener(simu);
		
		goButton.setActionCommand("go");
		stopButton.setActionCommand("stop");
		reloadButton.setActionCommand("reload");
		measButton.setActionCommand("meas");

		
		topControlPanel.add(goButton);
		topControlPanel.add(stopButton);
		//controlPanel.add(Box.createRigidArea(new Dimension(0, ((int)(0.3*yscale)))));
		botControlPanel.add(reloadButton);						 
		botControlPanel.add(measButton);
		
		controlPanel.add(topControlPanel, BorderLayout.NORTH);
		controlPanel.add(botControlPanel, BorderLayout.SOUTH);
		
		return controlPanel;
		
		
		
		
	}
	
	

	/**
	 * Erzeuge den Bereich mit dem SerialLog
	 * 
	 * @return JPanel des SerialLogs
	 */
	private JPanel createSerialLog() {
		panel = new JPanel(new BorderLayout());
		scrollPane = new JScrollPane(serialLog);
		serialLog.setRows(4); // Anzahl der Angezeigten Reihen
		serialLog.setAutoscrolls(true);
		serialLog.setForeground(Color.black);
		serialLog.setBackground(Color.white);
		serialLog.setFont(new Font("Arial", Font.BOLD, 15));
		DefaultCaret caret = (DefaultCaret) serialLog.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		clearButton = new JButton("Clear Serial Output");
		clearButton.setActionCommand("clearSerial");
		clearButton.addActionListener(this);
		panel.add(clearButton, BorderLayout.CENTER);

		checkBox = new JCheckBox("Autoscroll");
		checkBox.setActionCommand("autoscroll");
		checkBox.addActionListener(this);
		checkBox.setSelected(true);
		panel.add(checkBox, BorderLayout.EAST);

		panel.add(scrollPane, BorderLayout.PAGE_END);
		
		
		return panel;
	}
	
	
	/**
	 * Funktion wird aufgerufen wenn GUI upgedatet wird um das Seriallog anzupassen.
	 * @param size
	 * Aktuelle Gr��e Fenster in y
	 */
	public void updateSerialLogHeight(int size) {
		//Die Reihenanzahl des SerialLog wird anhand der Gr��e des oberen Panels berechnet
		int numRows =  Math.min(Math.max(((int)size/20)-3, 3), 20);
		serialLog.setRows(numRows);			
		this.setVisible(true);
	}

	/**
	 * Zeichne alle Verdrahtungen, welcher über die Modulgrenzen hinausgehen.<br>
	 * Diese Funktion ist sehr "Hardgecoded" eine Änderung an den Pin Zuordnungen
	 * wird hier Probleme verursachen.
	 * 
	 * @param g Graphics Instanz des Panels
	 */
	private void drawConnections(Graphics g) {
		
			
		
			
		
			// Zeichne die Stromversorgung
			for (int i = -1; i < 2; i++) {
				g.setColor(Color.BLACK);
				g.fillRect(i * ((int)(0.30372*xscale)) + ((int)(0.2699*xscale)), ((int)(0.18924*yscale)), ((int)(0.062*xscale)), ((int)(0.00516*yscale)));
				g.setColor(Color.RED);
				g.fillRect(i * ((int)(0.30372*xscale)) + ((int)(0.2699*xscale)), ((int)(0.2182*yscale)), ((int)(0.062*xscale)), ((int)(0.00516*yscale)));
			}
	
			// Zeichne die Signalleitungen
			int[] assignment = { 11, 10, 9, 4, 3, 2, 14 };
			g.setColor(Color.ORANGE);
			Vector<Point> pinArduino = modules[3].getPinPos();
			Point posArduino = modules[3].getPosition();
			int counter = 0;
			int lineWidth = Math.min(Math.max(((int)xscale/150), 1), 5);
            Stroke roundLine = new BasicStroke(lineWidth, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
			//Stroke roundLine = new BasicStroke(5, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);
			for (int i = 0; i < 3; i++) {
				Vector<Point> pos = modules[i].getPinPos();
				for (Point p : pos) {
					Graphics2D g2 = (Graphics2D) g;
					g2.setStroke(roundLine);
					if (counter > 5) { // Fall für die analoge Signalleitung
						g2.drawLine(p.x + modules[i].getPosition().x, p.y + modules[i].getPosition().y,
								p.x + modules[i].getPosition().x,
								pinArduino.get(assignment[counter]).y + posArduino.y - ((int)(0.0414*yscale)));
						g2.drawLine(p.x + modules[i].getPosition().x,
								pinArduino.get(assignment[counter]).y + posArduino.y - ((int)(0.0414*yscale)),
								pinArduino.get(assignment[counter]).x + posArduino.x,
								pinArduino.get(assignment[counter]).y + posArduino.y - ((int)(0.0414*yscale)));
						g2.drawLine(pinArduino.get(assignment[counter]).x + posArduino.x,
								pinArduino.get(assignment[counter]).y + posArduino.y - ((int)(0.0414*yscale)),
								pinArduino.get(assignment[counter]).x + posArduino.x,
								pinArduino.get(assignment[counter]).y + posArduino.y);
					} else { // Alle anderen Signalleitungen
						g2.drawLine(p.x + modules[i].getPosition().x, p.y + modules[i].getPosition().y,
								p.x + modules[i].getPosition().x, p.y + modules[i].getPosition().y + ((int)(0.09298*yscale)) - (counter) * ((int)(0.01033*yscale)));
						g2.drawLine(p.x + modules[i].getPosition().x,
								p.y + modules[i].getPosition().y + ((int)(0.09298*yscale)) - (counter) * ((int)(0.01033*yscale)),
								pinArduino.get(assignment[counter]).x + posArduino.x,
								p.y + modules[i].getPosition().y + ((int)(0.09298*yscale)) - (counter) * ((int)(0.01033*yscale)));
						g2.drawLine(pinArduino.get(assignment[counter]).x + posArduino.x,
								p.y + modules[i].getPosition().y + ((int)(0.09298*yscale)) - (counter) * ((int)(0.01033*yscale)),
								pinArduino.get(assignment[counter]).x + posArduino.x,
								pinArduino.get(assignment[counter]).y + posArduino.y);
					}
					counter++;
				}
			}
		
	}

	/**
	 * Funktion welche die GUI aktualisiert und auf 144Hz ausgelegt ist.
	 */
	public void run() {
		while (!stopFlag) {
			try {
				//Thread.sleep(7); // 144 Hz
				//Thread.sleep(17); //60 Hz
				Thread.sleep(50); //20 Hz
			} catch (InterruptedException e) {

				// e.printStackTrace();
			}
			super.repaint();
		}
	}

	/**
	 * Fügt eine neue Nachricht der SerialLog hinzu und löscht eventuell alte
	 * Nachrichten.
	 * 
	 * @param content Auszugebener String
	 * @return true
	 */
	public boolean serialPrint(String content) {
		serialprint.add(content);
		if (serialprint.size() > MAXIMUM_MESSAGES) {
			serialprint.remove(0);
		}
		String tmp = "";
		for (int i = 0; i < serialprint.size(); i++) {
			tmp = tmp + serialprint.get(i);
		}
		serialLog.setText(tmp);
		return true;
	}

	/**
	 * Verbindet alle Modulen mit ihren Pin Objekten auf dem Arduino.
	 * 
	 * @param arduino Instanz der Arduinoklasse
	 * @return true
	 */
	public boolean connectPins(Arduino arduino) {
		for (Modul modul : modules) {
			modul.connect(arduino);
		}
		return true;
	}

	public void stopThread() {
		stopFlag = true;
	}

	public void actionPerformed(ActionEvent arg0) {
		
		if (arg0.getActionCommand() == "clearSerial") {
			serialprint.clear();
			serialLog.setText("");
		} else if (arg0.getActionCommand() == "autoscroll") {
			if (((JCheckBox) arg0.getSource()).isSelected()) {
				DefaultCaret caret = (DefaultCaret) serialLog.getCaret();
				caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
			} else {
				DefaultCaret caret = (DefaultCaret) serialLog.getCaret();
				caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
			}
		}

	}
	





}
