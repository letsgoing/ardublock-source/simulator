/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.io;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;

/**
 * Liest den analogen Wert vom Pin [0V=0;5V=1023]
 * 
 * @author Lucas
 * 
 */
public class CodeAnalogRead extends SimCode {
	private SimTypeInt pin;

	public CodeAnalogRead(SimTypeInt _pin) {
		pin = _pin;
	}

	@Override
	public SimTypeInt run(Arduino _arduino, SimCode functionHead) {
		int ans = _arduino.analogRead(pin.run(_arduino, functionHead).getValue());
		return new SimTypeInt(ans);
	}

	@Override
	public String toString() {
		return "";
	}

}
