/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.io;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;

/**
 * Liest den digitalen Wert des Pins ein [HIGH/LOW]
 * 
 * @author Lucas
 * 
 */
public class CodeDigitalRead extends SimCode {
	private SimTypeInt pin;

	public CodeDigitalRead(SimTypeInt _pin) {
		pin = _pin;
	}

	@Override
	public SimTypeBool run(Arduino _arduino, SimCode functionHead) {
		boolean ans = _arduino.digitalRead(pin.run(_arduino, functionHead).getValue());
		return new SimTypeBool(ans);
	}

	@Override
	public String toString() {
		return "";
	}

}
