/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.io;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;

/**
 * Ausgang eines digitalen Ausgangs festlegen (HIGH/LOW)
 * 
 * @author Lucas
 * 
 */
public class CodeDigitalWrite extends SimCode {
	SimTypeInt pin;
	SimTypeBool value;

	public CodeDigitalWrite(SimTypeInt _pin, SimTypeBool _value) {
		pin = _pin;
		value = _value;
	}

	public SimCode run(Arduino _arduino, SimCode functionHead) {
		_arduino.digitalWrite(pin.run(_arduino, functionHead).getValue(), value.run(_arduino, functionHead).getValue());
		return null;
	}

	@Override
	public String toString() {
		return "";
	}

}
