/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.io;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;

/**
 * PWM-Ausgang Wert[0-255] zuweisen
 * 
 * @author Lucas
 * 
 */
public class CodeAnalogWrite extends SimCode {
	private SimTypeInt pin;
	private SimTypeInt value;

	public CodeAnalogWrite(SimTypeInt _pin, SimTypeInt _value) {
		pin = _pin;
		value = _value;
	}

	@Override
	public SimCode run(Arduino _arduino, SimCode functionHead) {
		_arduino.analogWrite(pin.run(_arduino, functionHead).getValue(), value.run(_arduino, functionHead).getValue());
		return null;
	}

	@Override
	public String toString() {
		return "";
	}

}
