/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.datatypes;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;

/**
 * Klasse für einen int16-Datentyp
 * 
 * @author Lucas, Anian
 * 
 */
public class SimTypeInt extends SimCode {
	private short value = 0;
	private SimCode followBlock;

	public SimTypeInt(int _value) {
		value = (short) _value;
	}

	public SimTypeInt(SimCode block) {
		followBlock = block;
	}

	@Override
	public SimTypeInt run(Arduino _arduino, SimCode functionHead) {
		if (followBlock instanceof SimCode) {		
				try {
					SimTypeInt ret = (SimTypeInt) followBlock.run(_arduino, functionHead);
					value = (short) ret.getValue();
				} catch (Exception e) {
					//if a none-SimTypeInt Block is attached 
					try {
						SimTypeLong ret = (SimTypeLong) followBlock.run(_arduino, functionHead);
						value = (short) ret.getValue();
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}	
		}
		return new SimTypeInt(value);
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	public int getValue() {
		return (int) value;
	}

}
