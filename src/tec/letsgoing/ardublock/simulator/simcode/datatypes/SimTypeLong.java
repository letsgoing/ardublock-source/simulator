/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.datatypes;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;

/**
 * Klasse für einen Int32-Datentyp (long)
 * 
 * @author Anian
 * 
 */
public class SimTypeLong extends SimCode {
	private int value = 0;
	private SimCode followBlock;

	public SimTypeLong(int _value) {
		value = (int) _value;
	}

	public SimTypeLong(SimCode block) {
		followBlock = block;
	}

	@Override
	public SimTypeLong run(Arduino _arduino, SimCode functionHead) {
		if (followBlock instanceof SimCode) {
			try {
				SimTypeLong ret = (SimTypeLong) followBlock.run(_arduino, functionHead);
				value = (int) ret.getValue();
			} catch (Exception e) {
				//if a none-SimTypeLong Block is attached 
				try {
					SimTypeInt ret = (SimTypeInt) followBlock.run(_arduino, functionHead);
					value = (short) ret.getValue();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}	
		}
		return new SimTypeLong(value);

	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	public int getValue() {
		return (int) value;
	}

}
