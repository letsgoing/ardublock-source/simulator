/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.datatypes;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;

/**
 * Klasse für einen Zeichenketten Datentyp
 * 
 * @author Lucas
 * 
 */
public class SimTypeString extends SimCode {
	private String content;
	private SimCode followBlock;

	public SimTypeString(String _content) {
		content = _content;
	}

	public SimTypeString(SimCode block) {
		followBlock = block;
	}

	@Override
	public SimTypeString run(Arduino _arduino, SimCode functionHead) {
		if (followBlock instanceof SimCode) {
			SimCode ret = followBlock.run(_arduino, functionHead);
			content = ret.toString();
		}
		return new SimTypeString(content);
	}

	@Override
	public String toString() {
		return content;
	}

}
