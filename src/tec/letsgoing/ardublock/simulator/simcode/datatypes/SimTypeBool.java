/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.datatypes;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;

/**
 * Klasse für einen BooldatenTyp
 * 
 * @author Lucas
 * 
 */
public class SimTypeBool extends SimCode {
	private boolean value = false;
	private SimCode followBlock;

	public SimTypeBool(boolean _value) {
		value = _value;
	}

	public SimTypeBool(SimCode block) {
		followBlock = block;
	}

	@Override
	public SimTypeBool run(Arduino _arduino, SimCode functionHead) {
		if (followBlock instanceof SimCode) {
			SimTypeBool ret = (SimTypeBool) followBlock.run(_arduino, functionHead);
			value = ret.getValue();
		}
		return new SimTypeBool(value);
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	public boolean getValue() {
		return value;
	}

}
