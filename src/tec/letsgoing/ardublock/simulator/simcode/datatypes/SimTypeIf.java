/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.datatypes;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;

/**
 * Unterklasse für die If-Codeblöcke
 * 
 * @author Lucas
 * 
 * 
 *
 */
public abstract class SimTypeIf extends SimCode {

	@Override
	public abstract SimCode run(Arduino _arduino, SimCode functionHead);

	@Override
	public abstract String toString();

}
