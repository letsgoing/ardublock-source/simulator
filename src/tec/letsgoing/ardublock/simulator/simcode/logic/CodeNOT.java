/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.logic;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;

/**
 * WAHR wenn der Wert FALSCH ist.
 * 
 * @author Lucas
 *
 */
public class CodeNOT extends SimCode {
	private SimTypeBool b1;

	public CodeNOT(SimTypeBool _b1) {
		b1 = _b1;
	}

	@Override
	public SimTypeBool run(Arduino _arduino, SimCode functionHead) {
		if (!b1.run(_arduino, functionHead).getValue()) {
			return new SimTypeBool(true);
		} else {
			return new SimTypeBool(false);
		}
	}

	@Override
	public String toString() {
		return "";
	}

}
