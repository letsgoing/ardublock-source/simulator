/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.logic;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;

/**
 * WAHR wenn beide Int Werte gleich sind.
 * 
 * @author Lucas
 *
 */
public class CodeIntEqual extends SimCode {
	private SimTypeInt b1;
	private SimTypeInt b2;

	public CodeIntEqual(SimTypeInt _b1, SimTypeInt _b2) {
		b1 = _b1;
		b2 = _b2;
	}

	@Override
	public SimTypeBool run(Arduino _arduino, SimCode functionHead) {
		if (b1.run(_arduino, functionHead).getValue() == b2.run(_arduino, functionHead).getValue()) {
			return new SimTypeBool(true);
		} else {
			return new SimTypeBool(false);
		}
	}

	@Override
	public String toString() {
		return "";
	}

}
