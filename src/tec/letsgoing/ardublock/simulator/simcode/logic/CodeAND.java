/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.logic;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;

/**
 * WAHR wenn beide Bedingungen erfüllt sind.
 * 
 * @author Lucas
 *
 */
public class CodeAND extends SimCode {
	private SimTypeBool b1;
	private SimTypeBool b2;

	public CodeAND(SimTypeBool _b1, SimTypeBool _b2) {
		b1 = _b1;
		b2 = _b2;
	}

	@Override
	public SimTypeBool run(Arduino _arduino, SimCode functionHead) {
		if (b1.run(_arduino, functionHead).getValue() && b2.run(_arduino, functionHead).getValue()) {
			return new SimTypeBool(true);
		} else {
			return new SimTypeBool(false);
		}
	}

	@Override
	public String toString() {
		return "";
	}

}
