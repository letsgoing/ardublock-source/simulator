/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.logic;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;

/**
 * WAHR wenn die beiden Bool-Werte ungleich sind
 * 
 * @author Lucas
 *
 */
public class CodeBoolUnequal extends SimCode {
	private SimTypeBool b1;
	private SimTypeBool b2;

	public CodeBoolUnequal(SimTypeBool _b1, SimTypeBool _b2) {
		b1 = _b1;
		b2 = _b2;
	}

	@Override
	public SimTypeBool run(Arduino _arduino, SimCode functionHead) {
		if (b1.run(_arduino, functionHead).getValue() != b2.run(_arduino, functionHead).getValue()) {
			return new SimTypeBool(true);
		} else {
			return new SimTypeBool(false);
		}
	}

	@Override
	public String toString() {
		return "";
	}

}
