/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.logic;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeString;

/**
 * WAHR wenn beide Zeichenketten nicht identisch sind.
 * 
 * @author Lucas
 *
 */
public class CodeStringUnequal extends SimCode {
	private SimTypeString b1;
	private SimTypeString b2;

	public CodeStringUnequal(SimTypeString _b1, SimTypeString _b2) {
		b1 = _b1;
		b2 = _b2;
	}

	@Override
	public SimTypeBool run(Arduino _arduino, SimCode functionHead) {
		if (!b1.run(_arduino, functionHead).toString().equals(b2.run(_arduino, functionHead).toString())) {
			return new SimTypeBool(true);
		} else {
			return new SimTypeBool(false);
		}
	}

	@Override
	public String toString() {
		return "";
	}

}
