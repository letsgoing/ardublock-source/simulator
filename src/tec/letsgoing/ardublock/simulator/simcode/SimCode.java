/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode;

import java.util.Vector;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.arduino.Variable;

/**
 *
 * Jedes Codeelement erbt von dieser dieser Klasse. Durch die run()-Methode kann
 * jedes Code-Objekt ausgeführt werden.
 * 
 * @author Lucas
 * 
 *
 *
 */
public abstract class SimCode {
	protected Vector<Variable> vars = new Vector<Variable>();
	protected boolean isMain = false;
	protected Arduino arduino;
	protected SimCode prevStack = null;

	/**
	 * Hauptaufruf, welcher während der Simulation ausgeführt wird. Dieser enthält
	 * die komplette Logik des Blocks.
	 * 
	 * @param _arduino     Die Instanz des Arduinos, damit Zugriffe auf die Hardware
	 *                     möglich sind.
	 * @param functionHead Aktueller Funktionskopf um lokale Variablen zu
	 *                     ermöglichen.
	 * @return Ein SimCode Objekt sollte eines als "Ergebnis" entstehen. Ansonsten
	 *         null
	 */
	public abstract SimCode run(Arduino _arduino, SimCode functionHead);

	public abstract String toString();

	public void createVariable(String _name, SimCode _value) {
		if (!isMain) {
			vars.add(new Variable(_name));
			vars.lastElement().setValue(_value);
		} else {
			arduino.createVariable(_name, _value);
		}
	}

	public boolean setVariable(String _name, SimCode _value) {
		for (Variable var : vars) {
			if (var.getName().equals(_name)) {
				var.setValue(_value);
				return true;
			}
		}
		if (prevStack != null) {
			if (prevStack.setVariable(_name, _value))
				return true;
		}
		if (arduino.setVariable(_name, _value))
			return true;

		return false;
	}

	public SimCode readVariable(String _name) {

		for (Variable var : vars) {
			if (var.getName().equals(_name)) {
				return var.getValue();
			}
		}
		if (prevStack != null) {
			return prevStack.readVariable(_name);
		}
		return arduino.readVariable(_name);

	}

}
