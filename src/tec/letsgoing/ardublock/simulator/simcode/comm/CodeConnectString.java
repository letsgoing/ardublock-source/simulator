/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.comm;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeString;

/**
 * Verbinde zwei beliebige Blöcke miteinander zu einem String.
 * 
 * @author Lucas
 * 
 * 
 *
 */
public class CodeConnectString extends SimCode {
	private SimCode block1, block2;
	String out;

	public CodeConnectString(SimCode _block1, SimCode _block2) {
		block1 = _block1;
		block2 = _block2;
	}

	@Override
	public SimTypeString run(Arduino _arduino, SimCode functionHead) {
		if (block1 != null && block2 != null) {
			out = "" + block1.run(_arduino, functionHead).toString() + " "
					+ block2.run(_arduino, functionHead).toString();
		} else if (block1 == null && block2 != null) {
			out = "" + block2.run(_arduino, functionHead).toString();
		} else if (block1 != null && block2 == null) {
			out = "" + block1.run(_arduino, functionHead).toString();
		} else {
			out = "";
		}
		return new SimTypeString(out);
	}

	@Override
	public String toString() {
		return out;
	}

}
