/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.comm;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeString;

/**
 * Sende Nachricht via Serial
 * 
 * @author Lucas, Anian
 * 
 *
 */
public class CodeSerialPrint extends SimCode {
	private SimTypeString stringBlock;
	private SimTypeBool boolBlock;
	
	private String replaceEscapeCodes(String str) {
		int indexNL  = str.indexOf("\\n");
		int indexTab = str.indexOf("\\t");
		
		while(indexNL > -1) {
			str = str.substring(0, indexNL) + "\n" + str.substring(indexNL+2);
			indexNL =  str.indexOf("\\n");
		}
		while(indexTab > -1) {
			str = str.substring(0, indexTab) + "\t" + str.substring(indexTab+2);
			indexTab = str.indexOf("\\t");
		}	
		return str;
	}

	public CodeSerialPrint(SimTypeString _stringBlock, SimTypeBool _boolBlock) {
		stringBlock = _stringBlock;
		boolBlock = _boolBlock;
	}

	public SimCode run(Arduino _arduino, SimCode functionHead) {
		String content;
		
		content = stringBlock.run(_arduino, functionHead).toString();
		content = replaceEscapeCodes(content);
		
		if (boolBlock.run(_arduino, functionHead).getValue()) {
			content += "\n";
		}
		_arduino.serialPrint(content);
		try {
			Thread.sleep(1); // Übertragung benötigt Zeit und SPAM führt zu unresposivness der GUI
		} catch (InterruptedException e) {
		}
		return null;

	}

	@Override
	public String toString() {
		return "";
	}

}
