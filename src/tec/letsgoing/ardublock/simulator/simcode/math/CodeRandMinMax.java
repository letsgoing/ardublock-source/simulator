/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.math;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;

/**
 * Erzeuge eine Zufallszahl zwische min und max
 * 
 * @author Lucas
 *
 */
public class CodeRandMinMax extends SimCode {
	private SimTypeInt max;
	private SimTypeInt min;

	public CodeRandMinMax(SimTypeInt _min, SimTypeInt _max) {
		min = _min;
		max = _max;
	}

	@Override
	public SimTypeInt run(Arduino _arduino, SimCode functionHead) {
		int randMax = max.run(_arduino, functionHead).getValue();
		int randMin = min.run(_arduino, functionHead).getValue();
		return new SimTypeInt((int) Math.floor(Math.random() * (randMax - randMin)) + randMin);
	}

	@Override
	public String toString() {
		return "";
	}

}
