/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.math;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;

/**
 * Die Division zweier Zahlen
 * 
 * @author Lucas
 *
 */
public class CodeDivison extends SimCode {
	SimTypeInt block1, block2;
	int out;

	public CodeDivison(SimTypeInt _block1, SimTypeInt _block2) {
		block1 = _block1;
		block2 = _block2;
	}

	@Override
	public SimCode run(Arduino _arduino, SimCode functionHead) {
		out = block1.run(_arduino, functionHead).getValue() / block2.run(_arduino, functionHead).getValue();
		return new SimTypeInt(out);
	}

	@Override
	public String toString() {
		return String.valueOf(out);
	}

}
