/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.math;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;

/**
 * Ordne einen Wert aus dem Bereich "von" auf den Bereich "zu"
 * 
 * @author Lucas
 *
 */
public class CodeMap extends SimCode {
	private SimTypeInt value;
	private SimTypeInt fromLow;
	private SimTypeInt fromHigh;
	private SimTypeInt toLow;
	private SimTypeInt toHigh;

	/**
	 * @param value
	 * @param fromLow
	 * @param fromHigh
	 * @param toLow
	 * @param toHigh
	 */
	public CodeMap(SimTypeInt _value, SimTypeInt _fromLow, SimTypeInt _fromHigh, SimTypeInt _toLow,
			SimTypeInt _toHigh) {
		this.value = _value;
		this.fromLow = _fromLow;
		this.fromHigh = _fromHigh;
		this.toLow = _toLow;
		this.toHigh = _toHigh;
	}

	@Override
	public SimTypeInt run(Arduino _arduino, SimCode functionHead) {
		float v = value.run(_arduino, functionHead).getValue();
		float fL = fromLow.run(_arduino, functionHead).getValue();
		float fH = fromHigh.run(_arduino, functionHead).getValue();
		float tL = toLow.run(_arduino, functionHead).getValue();
		float tH = toHigh.run(_arduino, functionHead).getValue();
		return new SimTypeInt((int) ((v - fL) / (fH - fL) * (tH - tL) + tL));
	}

	@Override
	public String toString() {
		return "";
	}

}
