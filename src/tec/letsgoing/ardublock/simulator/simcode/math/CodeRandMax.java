/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.math;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;

/**
 * Erzeuge eine Zufallszahl zwischen 0 und max-1
 * 
 * @author Lucas
 *
 */
public class CodeRandMax extends SimCode {
	private SimTypeInt max;

	public CodeRandMax(SimTypeInt _max) {
		max = _max;
	}

	@Override
	public SimTypeInt run(Arduino _arduino, SimCode functionHead) {
		int randMax = max.run(_arduino, functionHead).getValue();
		return new SimTypeInt((int) Math.floor(Math.random() * randMax));
	}

	@Override
	public String toString() {
		return "";
	}

}
