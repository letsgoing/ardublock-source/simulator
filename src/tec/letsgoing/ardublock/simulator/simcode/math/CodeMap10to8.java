/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.math;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;

/**
 * Ordne Werte aus dem Bereich [0,1023] auf den Bereich von [0,255] zu. (10 Bit
 * zu 8 Bit)
 * 
 * @author Lucas
 *
 */
public class CodeMap10to8 extends SimCode {
	private SimTypeInt value;

	public CodeMap10to8(SimTypeInt _value) {
		value = _value;
	}

	@Override
	public SimTypeInt run(Arduino _arduino, SimCode functionHead) {
		return new SimTypeInt(value.run(_arduino, functionHead).getValue() / 4);
	}

	@Override
	public String toString() {
		return "";
	}

}
