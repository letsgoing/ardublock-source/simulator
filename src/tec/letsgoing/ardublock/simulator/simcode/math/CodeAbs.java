/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.math;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;

/**
 * Der Betrag einer Zahl
 * 
 * @author Lucas
 *
 */
public class CodeAbs extends SimCode {
	private SimTypeInt value;

	public CodeAbs(SimTypeInt _value) {
		value = _value;
	}

	@Override
	public SimTypeInt run(Arduino _arduino, SimCode functionHead) {
		return new SimTypeInt(Math.abs(value.run(_arduino, functionHead).getValue()));
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

}
