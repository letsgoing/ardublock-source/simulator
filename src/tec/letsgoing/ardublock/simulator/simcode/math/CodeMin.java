/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.math;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;

/**
 * Die kleinere Zahl der beiden Eingaben
 * 
 * @author Lucas
 *
 */
public class CodeMin extends SimCode {
	private SimTypeInt block1;
	private SimTypeInt block2;

	public CodeMin(SimTypeInt _block1, SimTypeInt _block2) {
		block1 = _block1;
		block2 = _block2;
	}

	@Override
	public SimTypeInt run(Arduino _arduino, SimCode functionHead) {
		return new SimTypeInt(
				Math.min(block1.run(_arduino, functionHead).getValue(), block2.run(_arduino, functionHead).getValue()));
	}

	@Override
	public String toString() {
		return "";
	}

}
