/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.math;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;

/**
 * Limitiere einen analogen Wert
 * 
 * @author Lucas
 *
 */
public class CodeLimit extends SimCode {
	private SimTypeInt value;
	private SimTypeInt lowerLimit;
	private SimTypeInt upperLimit;

	public CodeLimit(SimTypeInt _value, SimTypeInt _lowerLimit, SimTypeInt _upperLimit) {
		value = _value;
		lowerLimit = _lowerLimit;
		upperLimit = _upperLimit;
	}

	@Override
	public SimCode run(Arduino _arduino, SimCode functionHead) {
		int v = value.run(_arduino, functionHead).getValue();
		int low = lowerLimit.run(_arduino, functionHead).getValue();
		int high = upperLimit.run(_arduino, functionHead).getValue();
		if (v > high)
			v = high;
		if (v < low)
			v = low;
		return new SimTypeInt(v);
	}

	@Override
	public String toString() {
		return "";
	}

}
