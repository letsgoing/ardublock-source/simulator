/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.control;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;

/**
 * Warte Millisekunden.
 * 
 * @author Lucas
 * 
 */
public class CodeDelay extends SimCode {
	SimTypeInt codeBlock;

	public CodeDelay(SimTypeInt _codeBlock) {
		codeBlock = _codeBlock;
	}

	public SimCode run(Arduino _arduino, SimCode functionHead) {
		int duration = (int) codeBlock.run(_arduino, functionHead).getValue();
		if (!_arduino.getStop()) {
			try {
				Thread.sleep(duration);
			} catch (InterruptedException e) {
				// e.printStackTrace();
			}
		}
		return null;

	}

	@Override
	public String toString() {
		return "";
	}

}
