/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.control;

import java.util.Vector;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.arduino.Variable;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeString;

/**
 * Wiederhole die Befehle x-mal. Die Wiederholungen werden in der Variable
 * gespeichert.
 * 
 * @author Lucas
 * 
 */
public class CodeForCount extends SimCode {
	private SimTypeInt count;
	private SimTypeString varname;
	private Vector<SimCode> codeblocks;

	public CodeForCount(SimTypeInt _count, SimTypeString _varname, Vector<SimCode> _blocks) {
		count = _count;
		varname = _varname;
		codeblocks = _blocks;
	}

	@Override
	public SimCode run(Arduino _arduino, SimCode functionHead) {
		super.prevStack = functionHead;
		super.arduino = _arduino;
		super.vars = new Vector<Variable>();
		this.createVariable(varname.toString(), new SimTypeInt(0));
		for (int i = 0; i < count.run(_arduino, functionHead).getValue(); i++) {
			this.setVariable(varname.toString(), new SimTypeInt(i));
			for (SimCode block : codeblocks) {
				block.run(_arduino, this);
			}
		}
		return null;
	}

	@Override
	public String toString() {

		return "";
	}

}
