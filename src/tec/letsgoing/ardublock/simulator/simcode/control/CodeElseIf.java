/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.control;

import java.util.Vector;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.arduino.Variable;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeIf;

/**
 * Führe die Befehle aus wenn falls-Block FALSCH ist und Bedingung WAHR ist.
 * 
 * @author Lucas
 * 
 */
public class CodeElseIf extends SimTypeIf {
	private SimTypeIf follow;
	private SimTypeBool condition;
	private Vector<SimCode> codeBlocks;

	public CodeElseIf(SimTypeIf _follow, SimTypeBool _condition, Vector<SimCode> _vec) {
		follow = _follow;
		condition = _condition;
		codeBlocks = _vec;
	}

	@Override
	public SimTypeBool run(Arduino _arduino, SimCode functionHead) {
		if (condition.run(_arduino, functionHead).getValue()) {
			// Condition met.
			super.prevStack = functionHead;
			super.arduino = _arduino;
			super.vars = new Vector<Variable>();
			for (SimCode block : codeBlocks) {
				block.run(_arduino, this);
			}
			return null;
		} else {
			// Condition not met and different else can execute
			if (follow != null) {
				follow.run(_arduino, functionHead);
			}
			return null;
		}
	}

	@Override
	public String toString() {
		return null;
	}

}
