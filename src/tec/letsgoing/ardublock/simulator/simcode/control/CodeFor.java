/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.control;

import java.util.Vector;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.arduino.Variable;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;

/**
 * 
 * Wiederhole die Befehle x-mal.
 * 
 * @author Lucas
 * 
 */
public class CodeFor extends SimCode {
	SimTypeInt count;
	Vector<SimCode> codeBlocks;

	public CodeFor(SimTypeInt _count, Vector<SimCode> vec) {
		count = _count;
		codeBlocks = vec;
	}

	@Override
	public SimCode run(Arduino _arduino, SimCode functionHead) {
		super.prevStack = functionHead;
		super.arduino = _arduino;
		super.vars = new Vector<Variable>();
		for (int i = 0; i < count.run(_arduino, functionHead).getValue(); i++) {
			for (SimCode code : codeBlocks) {
				code.run(_arduino, this);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return null;
	}

}
