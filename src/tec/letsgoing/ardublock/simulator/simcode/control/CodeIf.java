/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.control;

import java.util.Vector;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.arduino.Variable;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeIf;

/**
 * Führe Befehle aus, wenn die Bedingung WAHR ist.
 * 
 * @author Lucas
 *
 * 
 */
public class CodeIf extends SimTypeIf {
	private SimTypeIf follow;
	private SimTypeBool condition;
	private Vector<SimCode> codeBlocks;

	public CodeIf(SimTypeIf _follow, SimTypeBool _condition, Vector<SimCode> _vec) {
		condition = _condition;
		codeBlocks = _vec;
		follow = _follow;
	}

	/**
	 * Returns True if the Condition is met or false if it isnt
	 */
	public SimCode run(Arduino _arduino, SimCode functionHead) {
		if (condition.run(_arduino, functionHead).getValue()) {
			super.prevStack = functionHead;
			super.arduino = _arduino;
			super.vars = new Vector<Variable>();
			for (SimCode block : codeBlocks) {
				block.run(_arduino, this);
			}
			return null;// True Case
		} else {
			if (follow != null) {
				follow.run(_arduino, functionHead);
			}
			return null; // False Case
		}

	}

	public String toString() {
		return null;
	}

}
