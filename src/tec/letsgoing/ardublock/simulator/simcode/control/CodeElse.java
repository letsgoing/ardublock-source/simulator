/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.control;

import java.util.Vector;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.arduino.Variable;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeIf;

/**
 * Führe Befehle aus, falls "falls"-Block FALSCH (und evtl. "sonst falls"-Block)
 * sind.
 * 
 * @author Lucas
 * 
 */
public class CodeElse extends SimTypeIf {
	private Vector<SimCode> codeBlocks;

	public CodeElse(Vector<SimCode> _vec) {
		codeBlocks = _vec;
	}

	@Override
	public SimCode run(Arduino _arduino, SimCode functionHead) {
		super.prevStack = functionHead;
		super.arduino = _arduino;
		super.vars = new Vector<Variable>();

		for (SimCode block : codeBlocks) {
			block.run(_arduino, this);
		}

		return null;
	}

	@Override
	public String toString() {
		return null;
	}

}
