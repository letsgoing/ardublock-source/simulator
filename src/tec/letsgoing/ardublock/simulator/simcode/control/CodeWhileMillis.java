/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.control;

import java.util.Vector;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.arduino.Variable;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeString;

/**
 * Befehle ausserhalb von mache werden nur alle x-Millisekunden ausgeführt\nDie
 * Startzeit jedes Durchgangs wird in der Variable gespeichert
 * 
 * @author Lucas
 * 
 */
public class CodeWhileMillis extends SimCode {
	private SimTypeInt duration;
	private SimTypeString varname;
	private Vector<SimCode> blocks;

	public CodeWhileMillis(SimTypeInt _duration, SimTypeString _varname, Vector<SimCode> _blocks) {
		duration = _duration;
		varname = _varname;
		blocks = _blocks;
	}

	@Override
	public SimCode run(Arduino _arduino, SimCode functionHead) {
		super.prevStack = functionHead;
		super.arduino = _arduino;
		super.vars = new Vector<Variable>();
		this.setVariable(varname.toString(), new SimTypeInt(_arduino.getMillis()));
		int startTime = _arduino.getMillis();
		int dura = duration.run(_arduino, functionHead).getValue();
		while (startTime + dura > _arduino.getMillis()) {

			for (SimCode block : blocks) {
				block.run(_arduino, this);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return "";
	}

}
