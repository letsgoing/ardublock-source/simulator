/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.control;

import java.util.Vector;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.arduino.Variable;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;

/**
 * Führe Befehle aus, solange die Bedingung WAHR ist.
 * 
 * @author Lucas
 * 
 */
public class CodeWhile extends SimCode {
	SimTypeBool condition;
	Vector<SimCode> codeBlocks;

	public CodeWhile(SimTypeBool cond, Vector<SimCode> vec) {
		condition = cond;
		codeBlocks = vec;
	}

	@Override
	public SimCode run(Arduino _arduino, SimCode functionHead) {
		while (!_arduino.getStop() && condition.run(_arduino, functionHead).getValue()) {
			super.prevStack = functionHead;
			super.arduino = _arduino;
			super.vars = new Vector<Variable>();
			for (SimCode code : codeBlocks) {
				code.run(_arduino, this);
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return null;
	}

}
