/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.functions;

import java.util.Vector;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.arduino.Variable;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;

/**
 * Erstellt ein Unterprogramm.
 * 
 * @author Lucas
 *
 */
public class SimCodeFunction extends SimCode {
	private String name;
	private Vector<SimCode> codeBlocks = new Vector<SimCode>();

	public SimCodeFunction(String _name, Vector<SimCode> vec) {
		name = _name;
		codeBlocks = vec;
	}

	public SimCode run(Arduino _arduino, SimCode functionHead) {
		if (functionHead == null) {
			super.isMain = true;
		} else {
			super.prevStack = functionHead;
		}

		super.arduino = _arduino;
		super.vars = new Vector<Variable>();
		for (SimCode code : codeBlocks) {
			code.run(_arduino, this);
		}
		return null;
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return null;
	}

}
