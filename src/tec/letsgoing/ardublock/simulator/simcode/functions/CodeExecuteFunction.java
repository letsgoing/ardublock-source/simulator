/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.functions;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;

/**
 * Führt das Unterprogramm mit dem Namen aus.
 * 
 * @author Lucas
 * 
 */
public class CodeExecuteFunction extends SimCode {
	private String name;

	public CodeExecuteFunction(String _name) {
		name = _name;
	}

	@Override
	public SimCode run(Arduino _arduino, SimCode functionHead) {
		_arduino.getFunction(name).run(_arduino, functionHead);
		return null;
	}

	@Override
	public String toString() {
		return "";
	}

}
