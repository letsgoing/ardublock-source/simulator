/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.vars;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeLong;

/**
 * Gibt die Zeit seit Programmstart in Millisekunden an
 * 
 * @author Lucas, Anian
 *
 */

//changed Datatype to SimTypeLong (int32)
public class CodeMillis extends SimCode {

	@Override
	public SimTypeLong run(Arduino _arduino, SimCode functionHead) {
		return new SimTypeLong(_arduino.getMillis());
	}

	@Override
	public String toString() {
		return null;
	}

}
