/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.vars;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeString;

/**
 * Lese eine boolsche Varibale
 * 
 * @author Lucas
 *
 */
public class CodeBoolGet extends SimCode {
	private SimTypeString var;

	public CodeBoolGet(SimTypeString _var) {
		var = _var;
	}

	@Override
	public SimTypeBool run(Arduino _arduino, SimCode functionHead) {
		return (SimTypeBool) functionHead.readVariable(var.run(_arduino, functionHead).toString());
	}

	@Override
	public String toString() {
		return null;
	}

}
