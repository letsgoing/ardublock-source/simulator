/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.vars;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeString;

/**
 * Lese eine analoge/Integer Variable
 * 
 * @author Lucas
 *
 */
public class CodeIntGet extends SimCode {
	private SimTypeString var;

	public CodeIntGet(SimTypeString _var) {
		var = _var;
	}

	@Override
	public SimTypeInt run(Arduino _arduino, SimCode functionHead) {
		return (SimTypeInt) functionHead.readVariable(var.run(_arduino, functionHead).toString());
	}

	@Override
	public String toString() {
		return null;
	}

}
