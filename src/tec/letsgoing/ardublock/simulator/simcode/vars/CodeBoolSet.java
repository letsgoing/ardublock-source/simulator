/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.vars;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeString;

/**
 * Setze eine boolsche Variable
 * 
 * @author Lucas
 *
 */
public class CodeBoolSet extends SimCode {
	private SimTypeString var;
	private SimTypeBool value;

	public CodeBoolSet(SimTypeString _var, SimTypeBool _value) {
		var = _var;
		value = _value;
	}

	@Override
	public SimCode run(Arduino _arduino, SimCode functionHead) {
		// Wenn die Variable nicht vorhanden ist, so erzeuge Sie.
		if (!functionHead.setVariable(var.run(_arduino, functionHead).toString(),
				new SimTypeBool(value.run(_arduino, functionHead).getValue()))) {
			functionHead.createVariable(var.run(_arduino, functionHead).toString(), value.run(_arduino, functionHead));
		}
		return null;
	}

	@Override
	public String toString() {
		return null;
	}

}
