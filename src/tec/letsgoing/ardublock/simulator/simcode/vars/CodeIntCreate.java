/**
 * 
 */
package tec.letsgoing.ardublock.simulator.simcode.vars;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeString;

/**
 * Erzeuge eine analoge/Integer Variable
 * 
 * @author Lucas
 *
 */
public class CodeIntCreate extends SimCode {
	private SimTypeString var;
	private SimTypeInt value;

	public CodeIntCreate(SimTypeString _var, SimTypeInt _value) {
		var = _var;
		value = _value;
	}

	@Override
	public SimCode run(Arduino _arduino, SimCode functionHead) {
		functionHead.createVariable(var.run(_arduino, functionHead).toString(), value.run(_arduino, functionHead));
		return null;
	}

	@Override
	public String toString() {
		return null;
	}

}
