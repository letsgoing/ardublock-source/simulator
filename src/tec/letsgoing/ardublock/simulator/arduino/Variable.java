/**
 * 
 */
package tec.letsgoing.ardublock.simulator.arduino;

import tec.letsgoing.ardublock.simulator.simcode.SimCode;

/**
 * Diese Klasse speichert eine Variable und stellt diese über Operationen zur
 * Verfügung
 * 
 * @author Lucas
 * 
 * 
 *
 */
public class Variable {

	private String name;
	private SimCode value;

	public Variable(String _name) {
		name = _name;
	}

	public String getName() {
		return name;
	}

	public void setValue(SimCode _value) {
		value = _value;
	}

	public SimCode getValue() {
		return value;
	}

}
