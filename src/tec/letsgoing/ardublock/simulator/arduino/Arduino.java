/**
 * 
 */
package tec.letsgoing.ardublock.simulator.arduino;

import java.util.Vector;

import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;
import tec.letsgoing.ardublock.simulator.simcode.functions.SimCodeFunction;
import tec.letsgoing.ardublock.simulator.view.GUI;

/**
 * Diese Klasse hält die Daten eines "virtuellen" Arduinos und verwaltet die
 * Hardware
 * 
 * @author Lucas
 */
public class Arduino {
	private GUI gui;
	private Pin[] pins = new Pin[21];
	private Vector<Variable> vars = new Vector<Variable>();
	private Vector<SimCodeFunction> functions = new Vector<SimCodeFunction>();
	private long startTime = 0;
	private volatile boolean stopFlag = false;

	/**
	 * Konstruktor der Klasse Arduino.
	 * 
	 * @param _gui Die GUI Instanz mit welcher die Hardware verknüpft wird.
	 */
	public Arduino(GUI _gui) {
		gui = _gui;
		for (int i = 0; i < 21; i++) {
			pins[i] = new Pin();
		}
	}

	/**
	 * Erzeugt eine globale Variable.
	 * 
	 * @param _name  Name der Variable
	 * @param _value Wert der Variable
	 */
	public void createVariable(String _name, SimCode _value) {
		vars.add(new Variable(_name));
		vars.lastElement().setValue(_value);

	}

	/**
	 * Setzt eine globale Variable
	 * 
	 * @param _name  Name der Variable
	 * @param _value Wert der Variable
	 * @return true, wenn die Variable gesetzt wurde - false, wenn die Variable
	 *         nicht vorhanden ist.
	 */
	public boolean setVariable(String _name, SimCode _value) {
		for (Variable var : vars) {
			if (var.getName().equals(_name)) {
				var.setValue(_value);
				return true;
			}
		}

		return false;
	}

	/**
	 * Liest den Wert einer globalen Variable. Sollte diese nicht existieren, so
	 * wird die Ausführung des Programms beendet.
	 * 
	 * @param _name Name der Variable
	 * @return SimCode die Variable
	 */
	public SimCode readVariable(String _name) {

		for (Variable var : vars) {
			if (var.getName().equals(_name)) {
				return var.getValue();

			}
		}
		this.errorAbort("Variable nicht definiert");
		return new SimTypeInt(0);
	}

	/**
	 * Die digitalWrite Funktion vom Arduino
	 * 
	 * @param _pin
	 * @param _value boolean
	 * @return true, wenn Pin korrekt - false, wenn es sich um einen falschen Pin
	 *         handelt.
	 * 
	 */
	public boolean digitalWrite(int _pin, boolean _value) {
		if (_pin > 19) {
			this.errorAbort("Write auf Pin aufgerufen, welcher nicht existiert");
			return false;
		}
		if (_value == true)
			pins[_pin].setValue(1023);
		else
			pins[_pin].setValue(0);
		return true;
	}

	/**
	 * Funktion um den digitalen Wert eines Pins zu lesen.
	 * 
	 * @param _pin Nummer des Pins
	 * @return boolean des Pin-Wertes
	 */
	public boolean digitalRead(int _pin) {
		if (_pin > 19) {
			this.errorAbort("Read auf Pin aufgerufen, welcher nicht existiert");
			return false;
		}
		if (pins[_pin].getValue() >= 512)
			return true;
		else
			return false;
	}

	/**
	 * Funktion für die analoge PWM Ausgabe.
	 * 
	 * @param _pin   Anzusteuernder Pin
	 * @param _value PWM-Wert
	 * @return true, für PWM Pin - false für nicht PWM-Pin
	 */
	public boolean analogWrite(int _pin, int _value) {
		int[] array = { 3, 5, 6, 9, 10, 11 };
		for (int i = 0; i < 6; i++) {
			if (_pin == array[i]) {
				pins[_pin].setValue((int) (_value * (1024.0 / 256.0)));
				// System.out.println(_value + " multi " + (int)(_value * (1024.0/256.0)));
				return true;
			}
		}

		return false;
	}

	/**
	 * Funktion um den analogen Wert eines Pins zu lesen
	 * 
	 * @param _pin Zahl ohne das Vorangestellte A
	 * @return Wert des Pins
	 */
	public int analogRead(int _pin) {
		if (_pin > 5) {
			this.errorAbort("AnalogRead auf nicht vorhandenen Pin angewendet");
		}
		return pins[_pin + 14].getValue();
	}

	/**
	 * Funktion um ein speziellen Pin als Objekt zu erhalten
	 * 
	 * @param number Absolute Nummer des Pins
	 * @return Pin-Objekt
	 */
	public Pin getPin(int number) {
		return pins[number];
	}

	/**
	 * Reset-Funktion welche alle Variabeln löscht und Pins zurücksetzt.
	 * 
	 * @return true
	 */
	public boolean reset() {
		vars = new Vector<Variable>();

		for (int i = 0; i < 21; i++) {
			pins[i].setValue(0);
		}
		return true;
	}

	/**
	 * Löscht die im Arduino gespeicherten Funktionen
	 */
	public void resetFunctions() {
		functions.clear();
	}

	/**
	 * Sendet einen Aufruf an die GUI für eine neue SerialPrint Zeile.
	 * 
	 * @param content String welcher ausgegeben werden soll.
	 */
	public void serialPrint(String content) {
		if (!stopFlag) {
			gui.serialPrint(content);
		}
	}

	/**
	 * Füge eine Funktion zum Funktionsspeicher hinzu
	 * 
	 * @param function SimCodeFunction Objekt, welches hinzugefügt werden soll.
	 */
	public void addFunction(SimCodeFunction function) {
		functions.add(function);
	}

	/**
	 * Speichere die aktuelle Zeit als startTime ab. <br>
	 * Dies wird benötigt, damit die Berechnung von millis() korrekt ist.
	 */
	public void setStartTime() {
		startTime = System.currentTimeMillis();
	}

	/**
	 * Berechnung der vergangenen Millisekunden seit Programmstart
	 * Note: Läuft doppelt so schnell über wie das millis() vom Arduino -> Nach etwa 25 Tagen
	 * @return Millisekunden seit Programmstart.
	 */
	public int getMillis() {
		return (int) (System.currentTimeMillis() - startTime); 
	}

	/**
	 * Rufe eine Funktion aus dem Funktionsspeicher des Arduino ab.
	 * 
	 * @param name Name der gesuchten Funktion
	 * @return SimCodeFunction-Objekt dieser Funktion
	 */
	public SimCodeFunction getFunction(String name) {
		for (SimCodeFunction function : functions) {
			if (function.getName().equals(name)) {
				return function;
			}

		}
		return null;
	}

	/**
	 * Setze das Stop-Flag auf einen boolean-Wert.
	 * 
	 * @param value true für Ausführungsstop
	 */
	public void setStop(boolean value) {
		stopFlag = value;
	}

	/**
	 * Lese das Stop-Flag
	 * 
	 * @return Wert des Stop-Flag
	 */
	public boolean getStop() {
		return stopFlag;
	}

	/**
	 * Funktion um die Ausführung intern zu beenden und eine Fehlermeldung im
	 * SerialLog anzuzeigen.
	 * 
	 * @param error Fehlerbeschreibung für das SerialLog.
	 */
	public void errorAbort(String error) {
		this.serialPrint("Fehler im Programm: " + error + " - Programm wird beendet\n");
		this.setStop(true);
		this.getPin(20).setValue(0);
	}

}
