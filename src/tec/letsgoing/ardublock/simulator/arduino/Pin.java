/**
 * 
 */
package tec.letsgoing.ardublock.simulator.arduino;

import java.util.Observable;
import java.util.Observer;

/**
 * Diese Klasse ist für einen Pin auf dem Arduino
 * 
 * @author Lucas
 * 
 * 
 *
 */
public class Pin extends Observable {
	// TODO Deprecated after Java 9 but still supported in Java 11
	private int value = 0;
	private boolean mode = false; // false == Input, true==Output

	public void setMode(boolean _mode) {
		mode = _mode;
	}

	public boolean getMode() {
		return mode;
	}

	public void setValue(int _value) {
		_value = _value % 1024;
		if (_value < 0)
			_value = 0;
		value = _value;
		this.setChanged();
		this.notifyObservers();
	}

	public int getValue() {
		return value;
	}

	public void setObserver(Observer observ) {
		this.addObserver(observ);
	}

}
