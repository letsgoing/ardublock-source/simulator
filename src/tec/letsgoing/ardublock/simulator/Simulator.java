/**
 * 
 */
package tec.letsgoing.ardublock.simulator;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import tec.letsgoing.ardublock.simulator.arduino.Arduino;
import tec.letsgoing.ardublock.simulator.simcode.SimCode;
import tec.letsgoing.ardublock.simulator.simcode.comm.CodeSerialPrint;
import tec.letsgoing.ardublock.simulator.simcode.control.CodeDelay;
import tec.letsgoing.ardublock.simulator.simcode.control.CodeWhile;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeBool;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeInt;
import tec.letsgoing.ardublock.simulator.simcode.datatypes.SimTypeString;
import tec.letsgoing.ardublock.simulator.simcode.functions.CodeExecuteFunction;
import tec.letsgoing.ardublock.simulator.simcode.functions.SimCodeFunction;
import tec.letsgoing.ardublock.simulator.simcode.io.CodeAnalogRead;
import tec.letsgoing.ardublock.simulator.simcode.io.CodeAnalogWrite;
import tec.letsgoing.ardublock.simulator.simcode.io.CodeDigitalWrite;
import tec.letsgoing.ardublock.simulator.view.GUI;

/**
 * Diese Klasse verwaltet alle Elemente des Simulators. Sie ist geschrieben als
 * Singelton. Daher wird die Instanz über getInstance geholt und der Konstruktor
 * ist private.<br>
 * 
 * Pinmapping: RGB: 9,10,11 Poti: A0 Button: 2,3,4
 * 
 * @author Lucas
 * 
 * 
 */
public class Simulator implements Runnable, ActionListener{
	
	private static final int X_SCALE              = 800;
	private static final int Y_SCALE              = 800;
	
	private static Simulator instance;
	private Arduino arduino;
	private GUI gui;
	private Thread guiThread;
	private Thread simuThread;
	private Vector<SimCodeFunction> functionsCode = new Vector<SimCodeFunction>();

	/**
	 * Privater Konstruktor der Klasse Simulator<br>
	 * Die Klasse ist als Singelton geschrieben, daher sollte diese Konstruktor nie
	 * direkt aufgerufen werden.
	 */
	private Simulator() {
		
		try {
			UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());

		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		createSubClasses(X_SCALE, Y_SCALE, null);
		
	}

	/**
	 * Funktion um die Instanz des Simulators zu erhalten. <br>
	 * Da es sich um einen Singelton handelt ist die Instanz bei jedem Aufruf die
	 * gleiche.
	 * 
	 * @return Instanz des Simulators
	 */
	public static synchronized Simulator getInstance() {
		if (instance == null) {
			instance = new Simulator();
		} else {
			instance.reload();
			
		}
		return instance;
	}

	/**
	 * Erzeugt die Instanzen für die Klassen GUI und Arduino und initialisiert
	 * diese.<br>
	 * Startet den GUI-Thread.
	 * 
	 * @return true
	 */
	private boolean createSubClasses(int _xscale, int _yscale, Point _windowLocation) {
		gui = new GUI(this, _xscale, _yscale, _windowLocation);
		arduino = new Arduino(gui);
		gui.connectPins(arduino);

		guiThread = new Thread(gui);
		guiThread.start();
		
		//override close operation
		//TODO: TEST
		gui.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				instance.stopSimu();
				gui.stopThread();
			}			  
		});

		return true;
	}

	/**
	 * Startet die Simulation<br>
	 * Wird durch den Play-Button gestartet.
	 */
	public void startSimu() {
		arduino.setStop(false);
		if (simuThread instanceof Thread) {
			if (!simuThread.isAlive()) {
				arduino.reset();
				arduino.setStartTime();
				simuThread = new Thread(this);
				simuThread.start();
			}
		} else {
			arduino.reset();
			arduino.setStartTime();
			simuThread = new Thread(this);
			simuThread.start();
		}

	}

	/**
	 * Funktion welche vom Thread ausgeführt wird, nachdem Thread.start() ausgeführt
	 * wurde.<br>
	 * Aktiviert die Power-LED und führt die main-Funktion im Arduino aus.
	 */
	@Override
	public void run() {
		arduino.getPin(20).setValue(1023); // Power ON LED
		arduino.setStop(false);
		arduino.getFunction("main").run(arduino, null);
		
	}

	/**
	 * Stoppt die Ausführung des Simulators.<br>
	 * Ruft dabei einem mal die Thread.interrupt() auf, weswegen alle sleep Aufrufe
	 * eine Interrupt Execption abfangen müssen.
	 */
	public void stopSimu() {
		arduino.setStop(true);
		if (simuThread instanceof Thread) {
			simuThread.interrupt();
			
		}
		arduino.getPin(20).setValue(0);

	}

	/**
	 * Setzt den Simulator wieder in den Ausgangszustand zurück.
	 * 
	 * @return true, wenn der Thread korrekt gestoppt wurde und false, wenn der
	 *         Thread nicht korrekt beendet wurde.
	 */
	public boolean reload() {
		
		
		if (simuThread instanceof Thread) {
			stopSimu();
			try {
				simuThread.join();
			} catch (InterruptedException e) {
				// e.printStackTrace();
				arduino.errorAbort("Thread Überwachung gestört - Bitte Programm neustarten");
				return false;
			}
		}
		//Beim Neustart des Simulators wird die bisherige Position und Gr��e des Fenster gespeichert
		Point locationWindow = gui.getLocation();
		
		
		
		//Bei unterschiedlicher
		//Die bisherige Gr��e des Fenster wird abgespeichert
		int widthWindow = gui.getWidth();
		int heightWindow = gui.getHeight();

		gui.stopThread();
		gui.dispose();

		
		//Die neue Objekte werden mit den gespeicherten Werten erzeugt
		createSubClasses(widthWindow, heightWindow, locationWindow);		
		//die GUI wird an die gespeicherte Position verschoben
		

		
		for (SimCodeFunction function : functionsCode) {
			arduino.addFunction(function);
		}	
		
		
		return true;
	}
	
	

	/**
	 * Führt einen Reset des Arduinos aus und startet diesen neu.
	 * 
	 * @return true
	 */
	public boolean reset() {
		if (simuThread instanceof Thread) {
			stopSimu();
			try {
				simuThread.join();
			} catch (InterruptedException e) {
			}
		}
		startSimu();
	
		return true;
	}

	/**
	 * Fügt dem Simulator eine neue Funktion hinzu. Diese wird sogleich auch dem
	 * Arduino übergeben.<br>
	 * Note: Es werden Funktionen nur hinzugefügt und nicht überschrieben. Sollte
	 * eine alte Funktion überschrieben werden, sollte zuvor resetFunctions()
	 * aufgerufen werden.
	 * 
	 * @param _functionsCode Ein SimCodeFunction Objekt, welches hinzugefügt werden
	 *                       soll
	 * @return true
	 */
	public boolean addFunctionsCode(SimCodeFunction _functionsCode) {
		functionsCode.add(_functionsCode);
		arduino.addFunction(_functionsCode);
		return true;
	}

	/**
	 * Funktion, welche den Funktionsspeicher in der Simulatorklasse und Arduino
	 * löscht.<br>
	 * Soll vor dem hochladen eines neuen Programms durchgeführt werden.<br>
	 * Vor dem ausführen muss der Simulator gestoppt werden, da sonst der Thread
	 * eine Nullpointer Exception hat.
	 * 
	 * @return true
	 */
	public boolean resetFunctions() {
		functionsCode.clear();
		arduino.resetFunctions();
		arduino.reset();
		return true;
	}

	/**
	 * Funktion, welche die GUI Interaktion verarbeitet und die entsprechenden
	 * Funktionen startet.
	 */
	public void actionPerformed(ActionEvent arg0) {
		String command = arg0.getActionCommand();
		if (command == "go") {
			this.startSimu();
		} else if (command == "stop") {
			this.stopSimu();
		} else if (command == "reset") {
			this.reset();
		} else if (command == "reload") {
			this.reload();
		} else if (command == "meas") {

		}
	}
	
	

	
		
	
	/**
	 * Demo-Main Funktion. Nicht Teil des normalen Simulators.
	 * 
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {
		Vector<SimCode> testSetup = new Vector<SimCode>();
		Vector<SimCode> testLoop = new Vector<SimCode>();
		int testdelay = 200;

		SimTypeBool b1 = new SimTypeBool(true);
		SimTypeBool b0 = new SimTypeBool(false);

		// SimTypeString s1 = new SimTypeString("CodeString Test");
		// SimTypeInt d1 = new SimTypeInt(9);
		SimTypeInt d5 = new SimTypeInt(5);
		SimTypeInt d3 = new SimTypeInt(0);
		SimTypeInt d9 = new SimTypeInt(9);
		SimTypeInt delay = new SimTypeInt(testdelay);
		// SimTypeString s2 = new SimTypeString(d1);
		// CodeAdd s5 = new CodeAdd(d1, d5);

		// testLoop.add(new CodeDigitalWrite(d2, b0));
		// testLoop.add(new CodeDelay(delay));
		// testLoop.add(new CodeDigitalWrite(d2, b1));
		// testLoop.add(new CodeDelay(delay));
		Vector<SimCode> forVec = new Vector<SimCode>();
		forVec.add(new CodeDigitalWrite(d3, b1));
		forVec.add(new CodeDelay(delay));
		forVec.add(new CodeDigitalWrite(d3, b0));
		forVec.add(new CodeDelay(delay));

		// testLoop.add(new CodeFor(d1, forVec));
		// testLoop.add(new CodeSerialPrint(new SimTypeString(new CodeMillis()), b1));
		testLoop.add(new CodeSerialPrint(new SimTypeString(new CodeAnalogRead(d5)), b1));
		testLoop.add(new CodeAnalogWrite(d9, new SimTypeInt(new CodeAnalogRead(d5))));
		testLoop.add(new CodeDelay(delay));

		SimCodeFunction setupCode = new SimCodeFunction("setup", testSetup);
		SimCodeFunction loopCode = new SimCodeFunction("loop", testLoop);

		Vector<SimCode> testMain = new Vector<SimCode>();
		testMain.add(new CodeExecuteFunction("setup"));
		Vector<SimCode> tempVec = new Vector<SimCode>();
		tempVec.add(new CodeExecuteFunction("loop"));
		testMain.add(new CodeWhile(b1, tempVec));
		SimCodeFunction main = new SimCodeFunction("main", testMain);

		Simulator simu = Simulator.getInstance();
		simu.addFunctionsCode(setupCode);
		simu.addFunctionsCode(loopCode);
		simu.addFunctionsCode(main);

	}



}
